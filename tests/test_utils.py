from itertools import product

import pytest

from fastapi_cbv.utils import get_generic_type, get_underlying_types


@pytest.mark.parametrize("_type", product((set, list, tuple), (str, int, float, bool)))
def test_get_generic_type__with_generic_type_pair(_type):
    generic_type = get_generic_type(_type[0][_type[1]])

    assert generic_type == _type[0]


def test_get_generic_type__with_multiple_subtypes():
    assert get_generic_type(tuple[str, int, bool, float]) == tuple


@pytest.mark.parametrize("_type", (set, list, dict, tuple))
def test_get_generic_type__with_no_concrete_subtype(_type):
    assert get_generic_type(_type) is None


@pytest.mark.parametrize("_type", (set, list, tuple, dict, int, float, str))
def test_get_subtypes__concrete_type(_type):
    assert get_underlying_types(_type) is None


@pytest.mark.parametrize("types", ((int,), (int, float), (int, str, list)))
def test_get_subtypes(types):
    subtypes = get_underlying_types(tuple[types])
    assert len(subtypes) == len(types)
    assert all(subtype in subtypes for subtype in types)
