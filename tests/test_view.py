import pytest
from fastapi import APIRouter, FastAPI, status

from fastapi_cbv.endpoint import Method, endpoint
from fastapi_cbv.view import view
from tests.utils import Factory


def decorate_view(
    app: FastAPI,
    router: APIRouter | None,
    cbv: object,
    default_status_code: int = status.HTTP_200_OK,
):
    view(router or app, default_status_code=default_status_code)(cbv)
    if router:
        app.include_router(router)


@pytest.fixture(name="application")
def fixture_application():
    yield FastAPI()


@pytest.fixture(name="router_factory")
def fixture_router_factory():
    def _factory(prefix: str = ""):
        router = APIRouter(prefix=prefix)
        return router

    return _factory


@pytest.fixture(name="cbv_factory")
def fixture_cbv_factory():
    def _factory(
        data: dict | None = None,
        methods=None,
        name: str = "TestClassBasedView",
        endpoints=None,
    ):
        data = data or {}
        methods = methods or []
        endpoints = endpoints or []
        for method in methods:

            def dummy(self):  # noqa # pylint: disable=unused-argument
                ...

            dummy.__name__ = method.value
            data[method.value] = dummy
        for _endpoint in endpoints:

            def dummy(self):  # noqa # pylint: disable=unused-argument
                ...

            data[_endpoint.get("alternative_name") or dummy.__name__] = _endpoint[
                "decorator"
            ](dummy)
        cbv = type(name, (object,), data)

        return cbv

    return _factory


def test_view__use_name_of_functions_as_methods(
    application: FastAPI,
    cbv_factory: Factory[object],
    router_factory: Factory[APIRouter],
):
    router = router_factory()
    cbv = cbv_factory(methods=Method)
    decorate_view(application, router, cbv)
    schema = application.openapi()
    for method in Method:
        assert "/" in schema["paths"]
        assert method.value in schema["paths"]["/"]
        assert (
            schema["paths"]["/"][method.value]["summary"]
            == f"{method.value.capitalize()} Test Class Based"
        )
        responses = schema["paths"]["/"][method.value]["responses"]
        assert "200" in responses


def test_view__use_name_in_endpoint_decorator(
    application: FastAPI,
    cbv_factory: Factory[object],
    router_factory: Factory[APIRouter],
):
    router = router_factory()
    cbv = cbv_factory(
        endpoints=[
            {
                "decorator": endpoint(
                    methods=["POST"], name="Edit Test Class Based", path="edit"
                )
            }
        ]
    )
    decorate_view(application, router, cbv)
    schema = application.openapi()
    assert "/edit" in schema["paths"]
    assert "post" in schema["paths"]["/edit"]
    assert schema["paths"]["/edit"]["post"]["summary"] == "Edit Test Class Based"


def test_view__default_status_code(
    application: FastAPI,
    cbv_factory: Factory[object],
    router_factory: Factory[APIRouter],
):
    router = router_factory()
    cbv = cbv_factory(endpoints=[{"decorator": endpoint(methods=["GET"])}])
    decorate_view(
        application, router, cbv, default_status_code=status.HTTP_405_METHOD_NOT_ALLOWED
    )
    schema = application.openapi()
    assert (
        str(status.HTTP_405_METHOD_NOT_ALLOWED)
        in schema["paths"]["/"]["get"]["responses"]
    )


def test_view__custom_status_codes(
    application: FastAPI,
    cbv_factory: Factory[object],
    router_factory: Factory[APIRouter],
):
    router = router_factory()
    cbv = cbv_factory(
        endpoints=[
            {"decorator": endpoint(methods=["GET"]), "alternative_name": "get"},
            {
                "decorator": endpoint(
                    methods=["POST"], status_code=status.HTTP_201_CREATED
                ),
                "alternative_name": "post",
            },
            {
                "decorator": endpoint(
                    methods=["GET"],
                    path="redirect",
                    status_code=status.HTTP_307_TEMPORARY_REDIRECT,
                )
            },
        ]
    )
    decorate_view(application, router, cbv)
    schema = application.openapi()
    for route, method, expected in (
        ("/", "get", status.HTTP_200_OK),
        ("/", "post", status.HTTP_201_CREATED),
        ("/redirect", "get", status.HTTP_307_TEMPORARY_REDIRECT),
    ):
        responses = schema["paths"][route][method]["responses"]
        assert len(responses) == 1
        assert str(expected) in responses
