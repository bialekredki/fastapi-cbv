import pytest
from fastapi import HTTPException

from fastapi_cbv.exception.factory import FormattedMessageExceptionFactory
from fastapi_cbv.openapi import DescribedExceptionModel, exceptions_to_responses


def test_exceptions_to_responses__exception():
    assert exceptions_to_responses({HTTPException(400, "test")}) == {
        400: {"description": "test", "model": DescribedExceptionModel}
    }


def test_exceptions_to_responses__function_exception_factory():
    assert exceptions_to_responses({lambda: HTTPException(400, "test")}) == {
        400: {"description": "test", "model": DescribedExceptionModel}
    }


def test_exceptions_to_responses__mixed():
    assert exceptions_to_responses(
        {lambda: HTTPException(404, "test"), HTTPException(400, "test1")}
    ) == {
        400: {"description": "test1", "model": DescribedExceptionModel},
        404: {"description": "test", "model": DescribedExceptionModel},
    }


def test_exceptions_to_responses__collision_on_status_code():
    assert exceptions_to_responses(
        [lambda: HTTPException(400, "lambda"), HTTPException(400, "exc")]
    ) == {
        400: {"description": "lambda or exc", "model": DescribedExceptionModel},
    }


def test_exceptions_to_response__broad_exception():
    assert exceptions_to_responses({Exception()}) == {
        400: {"description": "", "model": DescribedExceptionModel}
    }


@pytest.mark.parametrize("data", ("test", 5, 3.14, (1, 2, 3)))
def test_exception_to_response__random_data(data):
    assert exceptions_to_responses({data}) == {
        400: {"description": str(data), "model": DescribedExceptionModel}
    }


def test_exceptions_to_responses__exception_factory():
    assert exceptions_to_responses(
        {FormattedMessageExceptionFactory(exceptions=[(400, "test")])}
    ) == {400: {"description": "test", "model": DescribedExceptionModel}}
