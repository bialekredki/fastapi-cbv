# FastAPI Class Based Views

An utility package for FastAPI based application that provides tools for using class based views.

## Setup

You need `python>=3.10` to run this package.

This project depends on `fastapi` and `pydantic` libraries.

## Examples

#### Simple CBV

This example present the simplest possible implementation.

```python

from fastapi import FastAPI, APIRouter, Query
from pydantic import BaseModel

from fastapi_cbv.views import view

app = FastAPI()
router = APIRouter()

class UserModel(BaseModel):
    id: int
    email: str

@view(router)
class UserView:

    async def get(self, query: str = Query(), limit: int = 50, offset: int = 0):
        # do something

    def post(self, user: UserModel):
        # do something

```

#### Adding response model, response class and exceptions

This example presents how to use `view` decorator to get exceptions, and response model/class in OpenAPI documentation.

**_CAVEAT!_**
Exception in list need to be either function that return `HTTPException` or `HTTPException` itself. In case of a function it is required to have all of it's arguments to be optional.

```python

from fastapi import HTTPException, status
from fastapi.responses import PlainTextResponse
from pydantic import BaseModel

from fastapi_cbv.view import view

NotAuthorizedException = HTTPException(401, "Not authorized.")
NotAllowedException = HTTPException(405, "Method not allowed.")
not_found_exc_factory = lambda username="username": HTTPException(404, f"Not found user with username {username}")

class UserResponse(BaseModel):
    field: str | None = None

@view(router)
class MyView:
    exceptions = {"__all__": [NotAuthorizedException], "put": [NotAllowedException, not_found_exc_factory]}

    RESPONSE_MODEL = {"put": UserResponse}

    RESPONSE_CLASS = {"delete": PlainTextResponse}

    def get(self): pass
    def put(self): pass
    def delete(self): pass
```

#### More custom endpoints in class based view

```python
from fastapi import HTTPException, status
from fastapi.responses import PlainTextResponse
from pydantic import BaseModel

from fastapi_cbv.view import view
from fastapi_cbv.endpoint import endpoint

NotAuthorizedException = HTTPException(401, "Not authorized.")
NotAllowedException = HTTPException(405, "Method not allowed.")
not_found_exc_factory = lambda username="username": HTTPException(404, f"Not found user with username {username}")
SomeException = HTTPException(400, "Example.")

class UserResponse(BaseModel):
    field: str | None = None

@view(router)
class MyView:
    exceptions = {"__all__": [NotAuthorizedException], "put": [NotAllowedException, not_found_exc_factory], "edit": [SomeException]}

    RESPONSE_MODEL = {"put": UserResponse, "edit": UserResponse}

    RESPONSE_CLASS = {"delete": PlainTextResponse}

    def get(self): pass
    def put(self): pass
    def delete(self): pass
    @endpoint(("PUT",), path="edit")
    def edit(self): pass

```
