import logging
from collections.abc import Callable, Iterable

from fastapi import HTTPException, status
from pydantic import BaseModel, Field

logger = logging.getLogger("fastapi_cbv")


class DescribedExceptionModel(BaseModel):
    detail: str = Field(..., description="Exception details.")


def exceptions_to_responses(
    exceptions: Iterable[HTTPException | Callable[..., HTTPException]],
):
    mapping = {}

    for exception in exceptions:
        try:
            exc = exception if isinstance(exception, HTTPException) else exception()
        except TypeError:
            logger.warning(
                "Exception %s was failed to be parsed. Make sure it's either an HTTPException instance or it's a factory function with arguments having default values."
            )
            exc = HTTPException(status.HTTP_400_BAD_REQUEST, detail=str(exception))
        if exc.status_code not in mapping:
            mapping[exc.status_code] = {
                "description": exc.detail,
                "model": DescribedExceptionModel,
            }
        else:
            mapping[exc.status_code]["description"] += f" or {exc.detail}"

    return mapping
