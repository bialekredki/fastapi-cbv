from fastapi_cbv.exception.factory import FormattedMessageExceptionFactory

__all__ = ["FormattedMessageExceptionFactory"]
