import re
from collections.abc import Callable, Iterable

from fastapi import APIRouter, FastAPI, HTTPException, status
from fastapi.responses import JSONResponse

from fastapi_cbv.constants import (
    COMMON_KEYWORD,
    ENDPOINT_METADATA_ATTRIBUTE_NAME,
    EXCEPTIONS_ATTRIBUTE_NAME,
    RESPONSE_CLASS_ATTRIBUTE_NAME,
    RESPONSE_MODEL_ATTRIBUTE_NAME,
)
from fastapi_cbv.endpoint import Metadata, Method
from fastapi_cbv.openapi import exceptions_to_responses


def _view_class_name_default_parser(cls: object, method: str):
    class_name = " ".join(re.findall(r"[A-Z][^A-Z]*", cls.__name__.replace("View", "")))
    return method.capitalize() + " " + class_name


def view(
    router: FastAPI | APIRouter,
    *,
    path: str = "/",
    default_status_code: int = status.HTTP_200_OK,
    name_parser: Callable[[object, str], str] = _view_class_name_default_parser
):
    def _decorator(cls) -> None:
        obj = cls()
        cls_based_response_model = getattr(obj, RESPONSE_MODEL_ATTRIBUTE_NAME, {})
        cls_based_response_class = getattr(obj, RESPONSE_CLASS_ATTRIBUTE_NAME, {})
        common_exceptions = getattr(obj, EXCEPTIONS_ATTRIBUTE_NAME, {}).get(
            COMMON_KEYWORD, tuple()
        )
        for _callable_name in dir(obj):
            _callable = getattr(obj, _callable_name)
            if _callable_name in set(Method) or hasattr(
                _callable, ENDPOINT_METADATA_ATTRIBUTE_NAME
            ):
                metadata: Metadata = getattr(
                    _callable,
                    ENDPOINT_METADATA_ATTRIBUTE_NAME,
                    Metadata([_callable_name]),
                )
                exceptions: Iterable[HTTPException] = getattr(
                    obj, ENDPOINT_METADATA_ATTRIBUTE_NAME, {}
                ).get(_callable_name, [])
                exceptions += common_exceptions
                _path = path
                if metadata and metadata.path:
                    _path = path + metadata.path
                router.add_api_route(
                    _path,
                    _callable,
                    methods=list(metadata.methods),
                    response_class=metadata.response_class_or_default(
                        cls_based_response_class.get(_callable_name, JSONResponse)
                    ),
                    response_model=metadata.response_model_or_default(
                        cls_based_response_model.get(_callable_name)
                    ),
                    responses=exceptions_to_responses(exceptions),
                    name=metadata.name_or_default(name_parser(cls, _callable_name)),
                    status_code=metadata.status_code_or_default(default_status_code),
                )

    return _decorator
