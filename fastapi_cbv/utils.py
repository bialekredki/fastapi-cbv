import logging

logger = logging.getLogger("fastapi_cbv").addHandler(logging.NullHandler())


def get_generic_type(cls) -> type | None:
    return getattr(cls, "__origin__", None)


def get_underlying_types(cls) -> list[type] | None:
    return getattr(cls, "__args__", None)
