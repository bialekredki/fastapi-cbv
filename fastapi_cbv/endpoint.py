from collections.abc import Callable, Iterable
from dataclasses import dataclass
from enum import Enum
from functools import wraps
from typing import Any, ClassVar

from fastapi.responses import Response
from pydantic import BaseModel

from fastapi_cbv.utils import get_generic_type, get_underlying_types


class Method(str, Enum):
    GET = "get"
    POST = "post"
    PATCH = "patch"
    DELETE = "delete"
    PUT = "put"


@dataclass(frozen=True, init=True, repr=True)
class Metadata:
    methods: Iterable[Method]
    name: str | None = None
    path: str | None = None
    status_code: int | None = None
    response_model: type[BaseModel] | None = None
    response_class: type[Response] | None = None
    __default_method_suffix: ClassVar[str] = "_or_default"

    def __getattr__(self, __name: str) -> Any | Callable[[Any], Any]:
        if __name.endswith(Metadata.__default_method_suffix):
            prefix = __name.removesuffix(Metadata.__default_method_suffix)
            if hasattr(self, prefix):
                return lambda _default: getattr(self, prefix, None) or _default
            return getattr(self, prefix)
        raise AttributeError(f"{self.__class__.__name__} has no attribute {__name}")


def endpoint(
    methods: Iterable[str | Method] | None = None,
    *,
    name: str | None = None,
    path: str | None = None,
    status_code: int | None = None,
    response_model: type[BaseModel] | None = None,
    response_class: type[Response] | None = None,
):
    assert response_class is None or issubclass(
        response_class, Response
    ), f"Argument response_class needs to be a subclass of Response type. Got type {response_class}"

    if get_generic_type(response_model) and (
        underlying_types := get_underlying_types(response_model)
    ):
        assert any(issubclass(_type, BaseModel) for _type in underlying_types)
    elif get_generic_type(response_model) is None:
        assert response_model is None or issubclass(
            response_model, BaseModel
        ), f"Argument response_model needs to be a subclass of BaseModel type. Got type {response_model}"

    assert (
        isinstance(methods, Iterable)
        and not isinstance(methods, str)
        or methods is None
    ), "Methods passed to endpoint decorator are required to be iterable collection."

    def _decorator(function: Callable):
        @wraps(function)
        async def _wrapper(*args, **kwargs):
            return await function(*args, **kwargs)

        parsed_method = set()
        _methods = methods or ((name,) if name else (function.__name__,))
        for method in _methods:
            if isinstance(method, Method):
                parsed_method.add(method)
                continue
            try:
                parsed_method.add(Method[method.upper()])
            except KeyError as exc:
                raise ValueError(f"HTTP Method {method} is not allowed") from exc
        # pylint: disable=protected-access
        _wrapper.__endpoint_metadata = Metadata(
            methods=parsed_method,
            name=name,
            path=path,
            status_code=status_code,
            response_class=response_class,
            response_model=response_model,
        )
        return _wrapper

    return _decorator
