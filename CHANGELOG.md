## 0.0.1 (2023-05-05)

### Fix

- Fix throwing assertion error when generic containers are used as response_model

## 0.4.0 (2023-04-17)

### Feat

- Add custom exception factory class

### Fix

- Customizable status code
- Customizable status code

## 0.3.0 (2023-02-17)

### Feat

- Write endpoint decorator

## 0.0.1 (2023-02-25)

### Feat

- Add custom exception factory class

## 0.3.0 (2023-02-17)

### Feat

- Write endpoint decorator

## 0.0.1 (2023-02-17)

### Feat

- Write endpoint decorator

## 0.0.1 (2023-02-17)

### Feat

- Create simple barebones
